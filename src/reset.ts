import { info } from '@kominal/service-util/helper/log';
import service from '.';
import { TASK_ID } from '@kominal/service-util/helper/environment';
import { connections as activeConnections } from './connection';

export async function reset() {
	info('Starting reset job...');
	try {
		const connections = activeConnections.map((c) => {
			const { connectionId, userId } = c;
			return { connectionId, userId };
		});

		const smqClient = service.getSMQClient();
		if (smqClient) {
			smqClient.publish('QUEUE', 'STATUS.SYNC', 'RESET', { taskId: TASK_ID, connections });
			smqClient.publish('QUEUE', 'CALL.SYNC', 'RESET', { taskId: TASK_ID, connections });
			smqClient.publish('QUEUE', 'NOTIFICATION.SYNC', 'RESET', { taskId: TASK_ID, connections });
		}
	} catch (e) {
		console.log(e);
	}
}

import { Packet } from '@kominal/service-util/model/packet';
import { info } from '@kominal/service-util/helper/log';
import { connections, Connection } from './connection';
import service from '.';

export class SMQHandler {
	async onConnect(): Promise<void> {
		info('Connected to SwarmMQ cluster.');
		const smqClient = service.getSMQClient();
		if (smqClient) {
			connections
				.filter((c) => c.userId != undefined)
				.forEach((c) => {
					const { connectionId, userId } = c;
					smqClient.subscribe('TOPIC', `DIRECT.CONNECTION.${connectionId}`);
					smqClient.subscribe('TOPIC', `DIRECT.USER.${userId}`);
					smqClient.subscribe('TOPIC', `INTERNAL.REFRESH.${userId}`);
					c.interestingGroupIds.forEach((id) => smqClient.subscribe('TOPIC', `DIRECT.GROUP.${id}`));
					c.interestingUserIds.forEach((id) => smqClient.subscribe('TOPIC', `PUBLIC.USER.${id}`));
				});
		}
	}

	async onDisconnect(): Promise<void> {
		info('Disconnected from SwarmMQ cluster.');
	}

	async onMessage(packet: Packet): Promise<void> {
		let targetConnections: Connection[] = [];

		const { destinationType, destinationName, event } = packet;
		if (destinationType === 'TOPIC' && destinationName.startsWith('DIRECT.USER.')) {
			const userId = destinationName.split('.')[2];
			targetConnections = connections.filter((c) => c.userId === userId);
		} else if (destinationType === 'TOPIC' && destinationName.startsWith('DIRECT.CONNECTION.')) {
			const connectionId = destinationName.split('.')[2];
			targetConnections = connections.filter((c) => c.connectionId === connectionId);
		} else if (destinationType === 'TOPIC' && destinationName.startsWith('DIRECT.GROUP.')) {
			const groupId = destinationName.split('.')[2];
			targetConnections = connections.filter((c) => c.interestingGroupIds.includes(groupId));
		} else if (destinationType === 'TOPIC' && destinationName.startsWith('PUBLIC.USER.')) {
			const userId = destinationName.split('.')[2];
			targetConnections = connections.filter((c) => c.interestingUserIds.includes(userId));
		} else if (destinationType === 'TOPIC' && destinationName.startsWith('INTERNAL.REFRESH.')) {
			const userId = destinationName.split('.')[2];
			targetConnections = connections.filter((c) => c.userId === userId);
			targetConnections.forEach((c) => c.publicInterestingValues());
			return;
		}

		targetConnections.forEach((c) => c.socket.emit('EVENT', event));
	}
}

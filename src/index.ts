import Service from '@kominal/service-util/helper/service';
import { Connection } from './connection';
import { SMQHandler } from './handler.smq';
import { sync } from './sync';
import { reset } from './reset';

const service = new Service({
	id: 'socket-service',
	name: 'Socket Service',
	description: 'Enables clients to connect to the SwarmMQ distribution backbone based.',
	jsonLimit: '16mb',
	routes: [],
	database: 'socket-service',
	squad: true,
	scheduler: [
		{ task: sync, squad: true, frequency: 30 },
		{ task: reset, squad: false, frequency: 300 },
	],
	socketHandler: async (socket) => {
		new Connection(socket);
	},
	swarmMQ: new SMQHandler(),
});
service.start();

export default service;
